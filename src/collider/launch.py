import subprocess
import os

from steam import Steam

from booze.X11 import XBU
import booze.engines
from booze.profiles import Profile

def run(argv):
    binary = argv[2:len(argv)]
    proton_bin = Steam().find_proton()
    
    profile = Profile(binary[0])
    
    desktop = False
    switches = []
    
    if profile.have_item():
        profile.esync()
        switches = profile.switches()
        desktop = profile.desktop()
    
    if desktop:
        desktop = "/desktop={},{}".format(binary[0].split('/')[-1:][0], XBU().resolution_string())
        subprocess.run([proton_bin, "waitforexitandrun", "explorer.exe", desktop] + binary + switches)
    else:
        subprocess.run([proton_bin, "waitforexitandrun"] + binary + switches)
