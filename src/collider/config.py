import os
from configparser import ConfigParser
from xdg import BaseDirectory

from booze import utils

class Config(ConfigParser):
    def __init__(self):
        super().__init__()
    
        self.__default = {
            "General": {
                "dll_overrides": "conhost.exe=d;"
            },
            
            "Debug": {
                "WINEDEBUG": "-all",
                "VKD3D_DEBUG": "",
                "DXVK_LOG_LEVEL": "none",
                "WINE_MONO_TRACE": "",
                "GST_DEBUG": "0"
            }
        }
            
        collaider_config = BaseDirectory.xdg_config_home + "/collaider/collaider.conf"
        
        if os.path.exists(collaider_config):
            self.read(collaider_config)
        else:
            print("collaider: config file not found, creating default!")
            
            utils.try_mkdir(BaseDirectory.xdg_config_home + "/collaider")
            
            for section in self.__default.keys():
                self.add_section(section)
                
                for key, value in self.__default[section].items():
                    self.set(section, key, value)
            
            with open(collaider_config, 'w') as f:
                self.write(f)

    def __get_boolean(self, value, fallback = None):
        if value == None:
            value = fallback
        
        if value == "true" or value == "1":
            return True
        
        if value == "false" or value == "0":
            return False

    def fallback_get(self, section, key):
        return self.get(section, key, fallback=self.__default[section][key])
        
    def dll_overrides(self):
        return self.fallback_get("General", "dll_overrides")
