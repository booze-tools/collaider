from glob import glob
import os
import collections
import getpass

import vdf

class Steam:
    def __init__(self):
        self.__steam_config = None
        self.__homepath = "/home/{}".format(getpass.getuser())
        
        config_path = self.__homepath + "/.steam/steam/config/config.vdf"
        with open(config_path, 'r') as vdf_file:
            self.__steam_config = vdf.parse(vdf_file, mapper=collections.OrderedDict)
            
        self.__cfg = self.__steam_config["InstallConfigStore"]["Software"]["Valve"]["Steam"]
        
    def steam_dirs(self) -> list:
        dirs = []
        
        dirs.append(self.__homepath + "/.steam/steam/steamapps")
        
        for i in range(1, 100):
            folder = self.__cfg.get("BaseInstallFolder_" + str(i))
            if folder != None:
                if os.path.exists(folder + "/steamapps"):
                    dirs.append(folder + "/steamapps")
                    
        return dirs
        
    def content_dirs(self) -> list:
        dirs = []
        
        dirs.append(self.__homepath + "/.steam/steam/steamapps/common")
        dirs.append(self.__homepath + "/.steam/steam/compatibilitytools.d")
        dirs.append("/usr/share/steam/compatibilitytools.d")
        
        for i in range(1, 100):
            folder = self.__cfg.get("BaseInstallFolder_" + str(i))
            if folder != None:
                if os.path.exists(folder + "/steamapps/common"):
                    dirs.append(folder + "/steamapps/common")
                
                if os.path.exists(folder + "/compatibilitytools.d"):
                    dirs.append(folder + "/compatibilitytools.d")
        
        return dirs
    
    def find_proton(self) -> str:
        default = self.__cfg["CompatToolMapping"]["0"]["name"]
        
        for d in self.__dirs():
            for proton in glob(d + "/*/proton"):
                proton_dir = proton.split("/")[-2]
                
                if "steamapps/common" in proton:
                    if proton_dir.replace(".", "").replace("0", "").replace("- ", "").replace(" ", "_").lower() == default:
                        return proton
                else:
                    return proton
        
        return None
 
